var express = require('express')
var app = express()
var opn = require('opn')
app.use(express.static('./dist'))

app.get('/', function (req, res) {
  res.send('Hello Vue')
})

app.listen(2333)
opn("http://localhost:2333/user/index.html")