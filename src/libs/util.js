Object.extend = function(...paras) {
    let options, name, src, copy, copyIsArray, clone,
        target = paras[ 0 ] || {},
        i = 1,
        length = paras.length,
        deep = false;
    if ( typeof target === "boolean" ) {
        deep = target;
        target = paras[ i ] || {};
        i++;
    }
    if ( typeof target !== "object" && Object.prototype.toString.call(source) !== '[object Function]') {
        target = {};
    }
    if ( i === length ) {
        target = this;
        i--;
    }
    while (i < length) {
        options = paras[i];
        if (options!== null ) {
            for ( name in options ) {
                src = target[ name ];
                copy = options[ name ];
                if ( target === copy ) {
                    continue;
                }
                clone=src || (copy.length !== null ? [] : {});
                if(deep && copy && typeof copy === 'object' && !copy.nodeType){
                    clone=src || (copy.length !== null ? [] : {});
                    target[ name ] = this.extend( deep, clone, copy );
                } else if ( copy !== undefined ) {
                    target[ name ] = copy;
                }
            }
        }
        i++;
    }
    return target;
};

let util = {

};
util.title = function (title) {
    title = title ? title + ' - Home' : 'tinyvue project';
    window.document.title = title;
};

util.dateFormat = function (dateInt) {
    let date = new Date();
    date.setTime(dateInt);
    const y = date.getFullYear();
    let m = date.getMonth() + 1;
    m = m < 10 ? '0' + m : m;
    let d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    return y + '-' + m + '-' + d;
};
util.getStaticPre = function (url) {
    if(url&&url.indexOf("//")===-1&&url.indexOf("/")===0){
        url="//static.tinyapp.top"+url;
    }
    return url||"";
};



util.doLogin=function(){
    location.assign("/user/login.html");
};
export default util;