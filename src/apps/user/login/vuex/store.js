import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// 需要维护的状态
const state = {
    apiHost:"http://demo.tinyapp.top"
};

const mutations = {

};

export default new Vuex.Store({
    state,
    mutations
});