
import Vue from 'vue';
import tinyvue from '@tiny/tinyvue';
import s from '@tiny/tinyvue/dist/styles/tinyvue.css';
import axios from "axios";
import Vuex from 'vuex';
import App from './app.vue';
import store from './vuex/store';
import Qs from 'qs';
axios.defaults.withCredentials=true;
Vue.prototype.$http = axios;
Vue.prototype.$qs = Qs;
Vue.use(Vuex);
Vue.use(tinyvue);

new Vue({
    el: '#app',
    store,
    render: h => h(App)
})
