import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
Vue.use(Vuex);

// 需要维护的状态
const state = {
    accountList: [],
    type: [],
    accountConfig: [],
    activeAccount: {},
    activeFlow: {},
    apiHost: "http://demo.tinyapp.top",
    isLogin: localStorage.getItem("isLogin"),
    currentUserId: localStorage.getItem("loginUserId"),
    accessList: {},

};

const mutations = {
    initAccessList(state, data){
        state.accessList = data;
    },
    initAccountConfig(state, data){
        state.accountConfig = data;
    },
    initAccountList(state, data){
        state.accountList = data.list;
    },
    addAccount(state, account){
        state.accountList.push(account);
    },
    deleteAccount(state, account){
        state.accountList.splice(state.accountList.indexOf(account), 1);

        // state.accountList.splice(account);
    },
    editAccount(state, accountObj){
        state.accountList.splice(accountObj.index, 1, accountObj.data);
    },

    logout(){
        localStorage.removeItem("isLogin");
        state.isLogin = false;
        axios.get(state.apiHost + "/logout").then(function () {
            localStorage.removeItem("isLogin");
            state.isLogin = false;
        }).catch(function (err) {
            console.log("111", err);
        });
    }
};

export default new Vuex.Store({
    state,
    mutations
});