
const routers = [
    {
        path: '/',
        meta: {
            title: '账本中心'
        },
        component: (resolve) => require(['./views/account_list.vue'], resolve)
    },
    {
        path: '/account/:accountId',
        meta: {
            title: '流水列表'
        },
        component: (resolve) => require(['./views/account.vue'], resolve)
    }

];
export default routers;