
import Vue from 'vue';
import tinyvue from '@tiny/tinyvue';
import VueRouter from 'vue-router';
import Routers from './router';
import Util from '../../../libs/util';
import App from './app.vue';
import '@tiny/tinyvue/dist/styles/tinyvue.css';
import '../../../assets/css/base.css';
import axios from "axios"
import Vuex from 'vuex'
import store from './vuex/store';
import Loading from "../../../components/loading/index";
import Qs from 'qs';
axios.defaults.withCredentials=true;

/*
axios.interceptors.request.use(
    config => {
        if (store.state.token) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
            config.headers.Authorization = `token ${store.state.token}`;
        }
        return config;
    },
    err => {
        return Promise.reject(err);
    });
*/



axios.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        if (error.response) {
            switch (error.response.status) {
                case 401:
                    Util.doLogin();
            }
        }
        return Promise.reject(error.response.data)   // 返回接口返回的错误信息
    });


Vue.prototype.$http = axios;
Vue.prototype.$qs = Qs;
Vue.use(VueRouter);
Vue.use(tinyvue);
Vue.use(Vuex);
Vue.use(Loading);



Vue.filter('nl2br', function (value) {
    return value.replace(/\n/ig, "<br/>");
});
Vue.filter('currency', function (value, _currency, decimals) {
    const digitsRE = /(\d{3})(?=\d)/g;
    value = parseFloat(value);
    if (!isFinite(value) || !value && value !== 0) return '';
    _currency = _currency  ? _currency : '';
    decimals = decimals ? decimals : 2;
    let stringified = Math.abs(value).toFixed(decimals);
    let _int = decimals ? stringified.slice(0, -1 - decimals) : stringified;
    let i = _int.length % 3;
    let head = i > 0 ? _int.slice(0, i) + (_int.length > 3 ? ',' : '') : '';
    let _float = decimals ? stringified.slice(-1 - decimals) : '';
    let sign = value < 0 ? '-' : '';
    return sign + _currency + head + _int.slice(i).replace(digitsRE, '$1,') + _float;
});
// 路由配置
const RouterConfig = {
    //mode: 'history',
    routes: Routers
};
let router = new VueRouter(RouterConfig);

router.beforeEach((to, from, next) => {
    tinyvue.LoadingBar.start();
    Util.title(to.meta.title);
    next();
});

router.afterEach((to, from, next) => {
    tinyvue.LoadingBar.finish();
    window.scrollTo(0, 0);
});
new Vue({
    router: router,
    store,
    render: h => h(App)
}).$mount('#app');

