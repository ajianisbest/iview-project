import Vue from 'vue';
import tinyvue from '@tiny/tinyvue';
import a from '@tiny/tinyvue/dist/styles/tinyvue.css';
import App from './app.vue';

Vue.use(tinyvue)

new Vue({
    el: '#app',
    render: h => h(App)
});